package com.ausfaller.examples.aggregation.sync;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Sorts;
import nl.altindag.ssl.SSLFactory;
import nl.altindag.ssl.util.PemUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.X509ExtendedKeyManager;
import javax.net.ssl.X509ExtendedTrustManager;
import java.nio.file.Paths;
import java.util.Arrays;

import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.sort;


public class Application {
    private static final String DB_NAME = "test";
    final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        Application app = new Application();
        app.run();
    }

    public void run() {
        logger.debug("Start example");

        ConnectionString connectionString = new ConnectionString("mongodb://user:password@m07.ausfaller.com:27017/");

        // We need an SSL context because this is an on-prem environment using self-signed certificates.
        X509ExtendedKeyManager keyManager = PemUtils.loadIdentityMaterial(Paths.get("/etc/ssl/narwhal.pem"));
        X509ExtendedTrustManager trustManager = PemUtils.loadTrustMaterial(Paths.get("/etc/ssl/rootCA.pem"));
        SSLFactory sslFactory = SSLFactory.builder()
                .withIdentityMaterial(keyManager)
                .withTrustMaterial(trustManager)
                .build();
        SSLContext sslContext = sslFactory.getSslContext();

        // Building settings for Mongo Client
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .applyToSslSettings(builder -> {
                    builder.enabled(true);
                    builder.invalidHostNameAllowed(true);
                    builder.context(sslContext);
                })
                .build();

        // Mongo Client creation
        try (MongoClient mongoClient = MongoClients.create(settings)) {
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> collection = database.getCollection("stocks");
            try {
                logger.info("Connected successfully to server.");

                Document document = collection.aggregate(
                        Arrays.asList(
                            group("$region", Accumulators.sum("tally", 1)),
                            sort(Sorts.descending("tally"))))
                        .first();  // <- this example only retrieves first document.

            } catch (MongoException me) {
                logger.info("An error occurred while attempting to run a command: " + me.getMessage());
            }
        }
        catch (MongoException e) {
            logger.info("An error occurred while creating a MongoClient object: " + e.getMessage());
        }
    }
}