plugins {
    id("java")
}

group = "com.ausfaller.examples.aggregation.sync"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.slf4j:slf4j-api:2.0.5")
    implementation("org.slf4j:slf4j-simple:2.0.5")
    implementation("io.github.hakky54:sslcontext-kickstart-for-pem:7.4.8")
    implementation(  "org.mongodb:mongodb-driver-sync:4.8.0" )

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.0")

}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}